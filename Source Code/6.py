import random

class Addition:

    def __init__(self):
        self.matrix = self.gen_matrix()

    def __add__(self, other):
        
        new_matrix = []
        for i in range(len(self.matrix)):
            if type(self.matrix[i]) == list:
                temp = []
                for j in range(len(self.matrix[i])):
                    sum_of_elements = self.matrix[i][j] + other.matrix[i][j]
                    temp.append(sum_of_elements)
                    
                new_matrix.append(temp)
        return new_matrix

    def __str__(self):
        return str(self.matrix)

    def gen_matrix(self):
        m = []
        temp = []
        for i in range(5):
            if i % 2 == 0 and i != 0:
                m.append(temp)
                temp = []
            temp.append(random.randint(0, 10))

        return m

m1 = Addition()
m2 = Addition()

print('Matrix 1 : ', m1)
print('Matrix 2 : ', m2)
print(m1 + m2)



